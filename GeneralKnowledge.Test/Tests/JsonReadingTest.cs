﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using GeneralKnowledge.Test.App.Models;
using Newtonsoft.Json;
using System.Text;
namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic data retrieval from JSON test
    /// </summary>
    public class JsonReadingTest : ITest
    {
        public string Name { get { return "JSON Reading Test"; } }


        private class RootObject
        {
            public List<SamplePoint> Samples;
        }

        public void Run()
        {
            var jsonData = Resources.SamplePoints;

            Console.WriteLine(Name);
            

            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z




            RootObject root = JsonConvert.DeserializeObject<RootObject>(Encoding.Default.GetString(jsonData));

            List<(string parameter, double LOW, double AVG, double MAX)> lstData = new List<(string, double, double, double)>();


            if (root.Samples.Count > 0)
            {


                double max = 0, min = 0, avg = 0;

                //temperature
                max = root.Samples.Max(m => m.Temperature);
                min = root.Samples.Min(m => m.Temperature);
                avg = root.Samples.Average(m => m.Temperature);
                lstData.Add(("Temperature", min, avg, max));

                //Ph
                max = root.Samples.Max(m => m.Ph);
                min = root.Samples.Min(m => m.Ph);
                avg = root.Samples.Average(m => m.Ph);
                lstData.Add(("Ph", min, avg, max));


                //Phosphate
                max = root.Samples.Max(m => m.Phosphate);
                min = root.Samples.Min(m => m.Phosphate);
                avg = root.Samples.Average(m => m.Phosphate);
                lstData.Add(("Phosphate", min, avg, max));



                //Chloride
                max = root.Samples.Max(m => m.Chloride);
                min = root.Samples.Min(m => m.Chloride);
                avg = root.Samples.Average(m => m.Chloride);
                lstData.Add(("Chloride", min, avg, max));

                //Nitrate
                max = root.Samples.Max(m => m.Nitrate);
                min = root.Samples.Min(m => m.Nitrate);
                avg = root.Samples.Average(m => m.Nitrate);
                lstData.Add(("Nitrate", min, avg, max));


                //var myPropertyInfo = root.Samples[0].GetType().GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);

                //foreach (var propertyInfo in myPropertyInfo.Where(type => type.PropertyType != typeof(DateTime)))
                //{
                //    var name = propertyInfo.Name;

                //    var x = root.Samples.FindAll(m =>
                //        m.GetType().GetProperties().Where(p => p.Name == name).Count()==1);

                //    var min1 = root.Samples.Min(m => m.GetType().GetProperties()[name]);




                //    //var min = root.Samples.Min(m => m.GetType().GetProperties().Where(p => p.Name == name).FirstOrDefault()?.Name);
                //    //var max = root.Samples.Max(m => m.GetType().GetProperties().Where(p => p.Name == name));
                //    //var avg=  root.Samples.Average(m => m.GetType().GetProperties().Where(p => p.Name == name));

                //}

            }






            PrintOverview(lstData);
        }

        private void PrintOverview(List<(string parameter, double LOW, double AVG, double MAX)> lstData)
        {
            lstData.ForEach(t =>
                {
                    Console.WriteLine($@"parameter = {t.parameter}, Low ={t.LOW}, Max= {t.MAX}, Average={t.AVG}");
                }

                );
        }
    }
}
