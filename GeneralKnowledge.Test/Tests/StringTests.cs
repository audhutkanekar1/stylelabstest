﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic string manipulation exercises
    /// </summary>
    public class StringTests : ITest
    {
        public void Run()
        {
            // TODO
            // Complete the methods below

            AnagramTest();
            Console.WriteLine("\n");
            GetUniqueCharsAndCount();
        }

        private void AnagramTest()
        {

            Console.WriteLine(@"Anagram Test");
            var word = "stop";
            var possibleAnagrams = new string[] { "test", "tops", "spin", "post", "mist", "step" };

            foreach (var possibleAnagram in possibleAnagrams)
            {
                Console.WriteLine($@"{word} > {possibleAnagram},{possibleAnagram.IsAnagram(word)}");
            }
        }

        private void GetUniqueCharsAndCount()
        {
            Console.WriteLine(@"Unique Chars And Count Test");
            var word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";


            // TODO
            // Write an algorithm that gets the unique characters of the word below 
            // and counts the number of occurrences for each character found


            IDictionary<char, int> charDict = new Dictionary<char, int>();
            var outInt = 0;


            foreach (var ch in word)
            {
                if (!charDict.TryGetValue(ch, out outInt))
                {
                    charDict.Add(new KeyValuePair<char, int>(ch, 1));
                }
                else
                {
                    charDict[ch]++;
                }
            }

            Console.WriteLine(@"Unique Characters");
            outInt = 1;
            foreach (var kvp in charDict)
            {
                Console.WriteLine($@"Unique Character { outInt++ } :  {kvp.Key}");
            }

            Console.WriteLine("\n");
            Console.WriteLine(@"Character Count");
            foreach (var kvp in charDict)
            {
                Console.WriteLine($@"Char: {kvp.Key}  Count: {kvp.Value}");
            }

        }
    }


    public static class StringExtensions
    {
        public static bool IsAnagram(this string a, string b)
        {
            if (a.Length != b.Length) return false;
            List<char> list1 = a.ToList();
            List<char> list2 = b.ToList();
            for (int i = 0; i < a.Length; i++)
            {
                if (!list2.Remove(list1[i])) return false;
            }

            return true;
        }
    }
}

