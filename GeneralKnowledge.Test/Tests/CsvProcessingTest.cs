﻿using System;
using GeneralKnowledge.Test.App.Models;
using LINQtoCSV;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using StyleLabs.DataAccess;
using StyleLabs.Repository;
using StyleLabs.Models;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        CsvFileDescription inputFileDescription = new CsvFileDescription
        {
            SeparatorChar = ',',
            FirstLineHasColumnNames = true
        };

        public void Run()
        {
            Console.WriteLine(@"CSV file Processing Test");

            // TODO
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted

            var csvFile = Resources.AssetImport;


            var fileLocation = System.Environment.CurrentDirectory.Replace(@"\bin\Debug", @"\Resources\AssetImport.csv");

            //var fileLocation = @"F:\\AssetImport.csv";
            StreamReader fs = new StreamReader(fileLocation);

            var cc = new CsvContext();

            IEnumerable<CsvAsset> csvAssets = cc.Read<CsvAsset>(fileLocation);



            Console.WriteLine($@"Total records in csv file : {csvAssets.Count<CsvAsset>()}");

            Console.WriteLine($@"Do you want to add the csv records in db? type Y for yes and N for no");
            string input = Console.ReadLine();
            if (input.ToUpper() == "Y")
            {

                Console.WriteLine($@"Bulk insert of assets into db started - time = {DateTime.Now}");
                var dbContext = new StyleLabsDbContext();
                dbContext.Database.ExecuteSqlCommand("TRUNCATE TABLE [Assets]");
                var _assetRepository = new AssetRepository(dbContext);

                var assetList = new List<Asset>();
               // dbContext.BulkInsert(assetList);

                foreach (var csvAsset in csvAssets)
                {

                   // if (_assetRepository.GetAssetByIdAsync(Guid.Parse(csvAsset.AssetId)).Result.AssetId == 0)
                    {

                        var dbAsset = new Asset
                        {
                            AssetGuid = Guid.Parse(csvAsset.AssetId),
                            FileName = csvAsset.FileName,
                            MimeType = csvAsset.MimeType,
                            CreatedBy = csvAsset.CreatedBy,
                            Email = csvAsset.Email,
                            Country = csvAsset.Country,
                            Description = csvAsset.Description,
                            AssetCreatedDate = DateTime.Now
                        };
                        assetList.Add(dbAsset);
                        //_assetRepository.Add(dbAsset);
                    }

                }

                if (assetList.Count>0)
                {
                    dbContext.Assets.AddRange(assetList);
                    dbContext.SaveChanges();
                }
                Console.WriteLine($@"Bulk insert of assets into db Completed! - time = {DateTime.Now}");
            }
            //foreach (CsvAsset item in assets) {
            //    var a1 = item.AssetId;
            //}
        }
    }

}
