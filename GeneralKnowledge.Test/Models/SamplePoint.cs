﻿using System;
using Newtonsoft.Json;


namespace GeneralKnowledge.Test.App.Models
{
    public class SamplePoint
    {
        [JsonProperty("date")]
        public DateTime SampleDate { get; set; }

        [JsonProperty("temperature")]
        public double Temperature { get; set; }

        [JsonProperty("pH")]
        public double Ph { get; set; }

        [JsonProperty("phosphate")]
        public double Phosphate { get; set; }

        [JsonProperty("chloride")]
        public double Chloride { get; set; }

        [JsonProperty("nitrate")]
        public double Nitrate { get; set; }
    }
}
