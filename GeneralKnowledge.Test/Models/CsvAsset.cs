﻿using LINQtoCSV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.Models
{
    public class CsvAsset
    {
        [CsvColumn(Name = "asset id", FieldIndex = 1)]
        public string AssetId { get; set; }

        [CsvColumn(Name = "file_name", FieldIndex = 2)]
        public string FileName { get; set; }

        [CsvColumn(Name = "mime_type", FieldIndex = 3)]
        public string MimeType { get; set; }
        [CsvColumn(Name = "created_by", FieldIndex = 4)]
        public string CreatedBy { get; set; }

        [CsvColumn(Name = "email", FieldIndex = 5)]
        public string Email { get; set; }

        [CsvColumn(Name = "country", FieldIndex = 6)]
        public string Country { get; set; }

        [CsvColumn(Name = "description", FieldIndex = 7)]
        public string Description { get; set; }

        //asset id,file_name,mime_type,created_by,email,country,description
    }
}
