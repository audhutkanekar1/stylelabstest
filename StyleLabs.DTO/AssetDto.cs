﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace StyleLabs.DTO
{
    public class AssetDto
    {
        public int AssetId { get; set; }

        [Required]
        public Guid AssetGuid { get; set; }
        [Required]
        public string FileName { get; set; }
        [Required]
        public string MimeType { get; set; }

        [Required]
        public string CreatedBy { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Country { get; set; }

        public string Description { get; set; }
 
    }
}
