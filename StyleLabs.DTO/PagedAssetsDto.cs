﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StyleLabs.DTO
{
  public  class PagedAssetsDto
    {
        public Int32 TotalAssets { get; set; }  
        public IEnumerable<AssetDto> Assets  { get; set; }
    }
}
