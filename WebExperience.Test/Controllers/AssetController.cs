﻿using System;
using StyleLabs.IRepository;
using System.Threading.Tasks;
using System.Web.Http;
using StyleLabs.Repository;
using StyleLabs.DataAccess;
using StyleLabs.DTO;
using StyleLabs.IService;
using StyleLabs.Models;
using WebExperience.Test.Filters;

namespace WebExperience.Test.Controllers
{

    [StyleLabExceptionFilterAttribute]
    public class AssetController : ApiController
    {
        // TODO
        // Create an API controller via REST to perform all CRUD operations on the asset objects created as part of the CSV processing test
        // Visualize the assets in a paged overview showing the title and created on field
        // Clicking an asset should navigate the user to a detail page showing all properties
        // Any data repository is permitted
        // Use a client MVVM framework



        private readonly IAssetService _service;

        public AssetController(IAssetService service)
        {
            _service = service;
        }


        [HttpPut]
        public async Task<IHttpActionResult> UpdateAsset(AssetDto asset)
        {
            if (ModelState.IsValid)
            {
                await _service.UpdateAsset(asset);
                return Ok(asset);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }



        [HttpPost]
        public async Task<IHttpActionResult> AddAsset(AssetDto asset)
        {
     

            if (ModelState.IsValid)
            {
                await _service.AddAsset(asset);
                return Ok(asset);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        [HttpDelete]
        public async Task<IHttpActionResult> DeleteAsset(int assetId)
        {
            await _service.DeleteAsset(assetId);
            return Ok("deleted successfully!");
        }


        [HttpGet]
        public async Task<IHttpActionResult> GetAsset(Guid assetId)
        {
            var asset = await _service.GetAsset(assetId);
            return Ok(asset);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetPagedAssets([FromUri]int pageNumber, [FromUri]int pageSize, [FromUri]string fileName = null)
        {
            var assets = await _service.GetPagedAssets(pageNumber, pageSize, fileName);
            return Ok(assets);
        }
    }
}
