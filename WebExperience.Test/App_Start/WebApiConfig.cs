﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using StyleLabs.DataAccess;
using StyleLabs.IRepository;
using StyleLabs.IService;
using StyleLabs.Models;
using StyleLabs.Repository;
using StyleLabs.Service;
using Unity;
using WebExperience.Test.DIResolver;
using System.Web.Http.Cors;

namespace WebExperience.Test
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //CORS
            var cors = new EnableCorsAttribute("http://localhost:4200", "*", "*");
            config.EnableCors(cors);

            // Unity configuration
            var container = new UnityContainer();
            container.RegisterType<IAssetService, AssetService>();
            container.RegisterType<IRepositoryBase<Asset>, AssetRepository>();
            container.RegisterType<IAssetRepository, AssetRepository>();
            container.RegisterType<DbContext, StyleLabsDbContext>();



            config.DependencyResolver = new UnityResolver(container);


            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
