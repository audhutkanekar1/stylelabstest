﻿using StyleLabs.DataAccess;
using StyleLabs.IRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace StyleLabs.Repository
{

    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {

        protected readonly DbContext _dbContext;

        public RepositoryBase(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual async Task<IEnumerable<T>> FindAllAsync()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> FindByConditionAsync(Expression<Func<T, bool>> expression)
        {
            return await _dbContext.Set<T>().Where(expression).ToListAsync();
        }

        public virtual T GetById(int id)
        {
            return _dbContext.Set<T>().Find(id);
        }



        public virtual IEnumerable<T> List()
        {
            return _dbContext.Set<T>().AsEnumerable();
        }

        public virtual IEnumerable<T> List(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return _dbContext.Set<T>()
                .Where(predicate)
                .AsEnumerable();
        }
        public virtual void Delete(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            _dbContext.SaveChanges();
        }

        public virtual void Edit(T entity, string[] unmodifiedProperties)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            foreach (var propertyName in unmodifiedProperties)
            {
                if (!(_dbContext.Entry(entity).Property(propertyName) is null))
                    _dbContext.Entry(entity).Property(propertyName).IsModified = false;
            }

            _dbContext.SaveChanges();
        }

        public virtual void Add(T entity)
        {
            _dbContext.Set<T>().Add(entity);
            _dbContext.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await this._dbContext.SaveChangesAsync();
        }

        public virtual Int32 GetRecordsCount()
        {
            return this._dbContext.Set<T>().Count();
        }
    }
}
