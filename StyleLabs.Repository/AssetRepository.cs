﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StyleLabs.DataAccess;
using StyleLabs.IRepository;
using StyleLabs.Models;

namespace StyleLabs.Repository
{
    public class AssetRepository: RepositoryBase<Asset>, IAssetRepository
    {
        public AssetRepository(DbContext dbContext) : base(dbContext)
        {
        }

        public async  Task<IEnumerable<Asset>> GetAllAssetAsync()
        {
            var assets = await FindAllAsync();
            return assets.OrderBy(x => x.AssetId);
        }

        public async Task<Asset> GetAssetByIdAsync(Guid assetId)
        {
            var asset = await FindByConditionAsync(a => a.AssetGuid.Equals(assetId));
            return asset.DefaultIfEmpty(new Asset())
                .FirstOrDefault();
        }

        public async Task<int> GetAssetCount(string fileName)
        {
           return await Task.Run(() => this._dbContext.Set<Asset>()
                .Where(a => string.IsNullOrEmpty(fileName) || a.FileName.Contains(fileName)).Count());
        }

        public async Task<IEnumerable<Asset>> GetPagedAssetAsync(int pageNumber, int pageSize,string fileName)
        {
            var assets = await Task.Run(()=> this._dbContext.Set<Asset>()
                .Where(a=> string.IsNullOrEmpty(fileName) || a.FileName.Contains(fileName) )
                .Distinct()
                .OrderBy(a => a.AssetId)
                .Skip(pageSize * (pageNumber))
                .Take(pageSize) );

            return assets;

        }

    
    }
}
