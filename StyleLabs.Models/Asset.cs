﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StyleLabs.Models
{
    public class Asset
    {
        public int AssetId { get; set; }

        public Guid AssetGuid { get; set; }

        public string FileName { get; set; }

        public string MimeType { get; set; }

        public string CreatedBy { get; set; }
        
        public string Email { get; set; }

        public string Country { get; set; }

        public string Description { get; set; }

        public DateTime? AssetCreatedDate { get; set; }

        public DateTime? AssetModifiedDate { get; set; }
    }
}
