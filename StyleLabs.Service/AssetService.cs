﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StyleLabs.DTO;
using StyleLabs.IRepository;
using StyleLabs.IService;
using StyleLabs.Models;

namespace StyleLabs.Service
{
    public class AssetService : IAssetService
    {
        private readonly IAssetRepository _assetRepository;
        public AssetService(IAssetRepository assetRepository)
        {
            _assetRepository = assetRepository;
        }
        public virtual async Task<AssetDto> GetAsset(Guid assetId)
        {
            var asset = await _assetRepository.GetAssetByIdAsync(assetId);
            return await this.MapDto(asset);
        }

        public virtual async Task<PagedAssetsDto> GetPagedAssets(int pageNumber, int pageSize, string fileName)
        {
            var pagedAssetsDto = new PagedAssetsDto();
            List<AssetDto> lstAssetDtos = new List<AssetDto>();
            var assets = await _assetRepository.GetPagedAssetAsync(pageNumber, pageSize, fileName);
            pagedAssetsDto.TotalAssets = await _assetRepository.GetAssetCount(fileName);
            foreach (var asset in assets)
            {
                lstAssetDtos.Add(await this.MapDto(asset));
            }

            pagedAssetsDto.Assets = lstAssetDtos;
            return pagedAssetsDto;
        }



        public virtual async Task AddAsset(AssetDto assetDto)
        {
            var asset = await this.MapEntity(assetDto);
            _assetRepository.Add(asset);
        }

        public virtual async Task UpdateAsset(AssetDto assetDto)
        {
            var asset = await this.MapEntity(assetDto);
            _assetRepository.Edit(asset, new string[] { "AssetCreatedDate" });
        }


        public virtual async Task DeleteAsset(Guid assetGuid)
        {
            var asset = await _assetRepository.GetAssetByIdAsync(assetGuid);
            if (!(asset is null))
            {
                _assetRepository.Delete(asset);
            }
        }

        public virtual async Task DeleteAsset(int assetId)
        {
            var asset = _assetRepository.GetById(assetId);
            if (!(asset is null))
            {
                  _assetRepository.Delete(asset);
            }
        }



        protected Task<AssetDto> MapDto(Asset asset)
        {
            if (asset.AssetId > 0)
            {
                return Task.FromResult(new AssetDto
                {
                    AssetId = asset.AssetId,
                    AssetGuid = asset.AssetGuid,
                    FileName = asset.FileName,
                    MimeType = asset.MimeType,
                    CreatedBy = asset.CreatedBy,
                    Email = asset.Email,
                    Country = asset.Country,
                    Description = asset.Description
                });
            }
            else
            {
                return null;
            }

        }


        protected Task<Asset> MapEntity(AssetDto assetDto)
        {
              return Task.FromResult(new Asset
                {
                    AssetId = assetDto.AssetId,
                    AssetGuid = assetDto.AssetGuid,
                    FileName = assetDto.FileName,
                    MimeType = assetDto.MimeType,
                    CreatedBy = assetDto.CreatedBy,
                    Email = assetDto.Email,
                    Country = assetDto.Country,
                    Description = assetDto.Description,
                    AssetModifiedDate = DateTime.Now

                });
            

        }
    }
}
