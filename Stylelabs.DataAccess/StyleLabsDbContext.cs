﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StyleLabs.Models;

namespace StyleLabs.DataAccess
{
    public class StyleLabsDbContext:DbContext
    {
        public StyleLabsDbContext() : base("DefaultConnection")
        {
            this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        public virtual System.Data.Entity.DbSet<Asset> Assets { get; set; }
    }
}
