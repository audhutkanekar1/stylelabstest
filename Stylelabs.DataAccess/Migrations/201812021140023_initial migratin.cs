namespace Stylelabs.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialmigratin : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Assets",
                c => new
                    {
                        AssetId = c.Int(nullable: false, identity: true),
                        AssetGuid = c.Guid(nullable: false),
                        FileName = c.String(),
                        MimeType = c.String(),
                        CreatedBy = c.String(),
                        Email = c.String(),
                        Country = c.String(),
                        Description = c.String(),
                        AssetCreatedDate = c.DateTime(),
                        AssetModifiedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.AssetId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Assets");
        }
    }
}
