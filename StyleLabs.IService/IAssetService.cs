﻿using StyleLabs.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StyleLabs.IService
{
    public interface IAssetService
    {
        Task<AssetDto> GetAsset(Guid assetId);

        Task AddAsset(AssetDto asset);

        Task UpdateAsset(AssetDto asset);

        Task DeleteAsset(Guid assetGuid);

        Task DeleteAsset(int assetId);

        Task<PagedAssetsDto> GetPagedAssets(int pageNumber, int pageSize,string fileName);
    }
}
