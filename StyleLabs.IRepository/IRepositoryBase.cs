﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace StyleLabs.IRepository
{
    public interface IRepositoryBase<T>
    {

        Task<IEnumerable<T>> FindAllAsync();
        Int32 GetRecordsCount();
        Task<IEnumerable<T>> FindByConditionAsync(Expression<Func<T, bool>> expression);
        T GetById(int id);
        void Add(T entity);
        void Delete(T entity);
        void Edit(T entity, string[] unmodifiedProperties);
        Task SaveAsync();
    }
}
