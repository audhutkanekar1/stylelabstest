﻿using StyleLabs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StyleLabs.IRepository
{
    public interface IAssetRepository:IRepositoryBase<Asset>
    {
        Task<IEnumerable<Asset>> GetAllAssetAsync();
        Task<IEnumerable<Asset>> GetPagedAssetAsync(int pageNumber,int pageSize,string fileName);
        Task<Int32> GetAssetCount(string fileName);
        Task<Asset> GetAssetByIdAsync(Guid assetId);
      
    }
}
